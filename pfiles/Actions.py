import happybase

class Actions(object):

    def getText(self, value):
        connection = happybase.Connection('hbase', 9090)
        table = connection.table('palavras')
        return table.row(value)