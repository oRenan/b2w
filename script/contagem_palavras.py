import pyspark
from pyspark import SparkConf
from pyspark.sql import SparkSession,functions

spark_conf = SparkConf().setAppName('Words')
spark = SparkSession.builder.config(conf=spark_conf).getOrCreate()

path = "/data/words"
fileWords = spark.read.format("text").option("header", "false").load(path)

fileWords = fileWords.select(
    functions.split(fileWords['value'],r'\b[\W\s]+').alias('quebrado'))

fileWords = fileWords.select(functions.explode(fileWords['quebrado']).alias('word'))
fileWords = fileWords.groupBy(fileWords['word']).count()
fileWords = fileWords.where(functions.length(fileWords['word'])>0)

import happybase
connection =happybase.Connection('hbase', 9090)

if 'palavras' not in connection.tables():
    connection.create_table('palavras', { 'valores': dict() } )
table=connection.table('palavras')

for word in fileWords.collect():
    table.put(word['word'],{'valores:qtd': str(word['count'])})