FROM openjdk:8

#Install softwares basics 
RUN apt-get update
RUN apt-get -y install supervisor python-pip net-tools vim
RUN pip install supervisor-stdout
RUN mkdir -p /var/log/supervisor

ENV VERSION 1.3.1

WORKDIR /opt/

RUN wget http://archive.apache.org/dist/hbase/${VERSION}/hbase-${VERSION}-bin.tar.gz
RUN tar -xvf hbase-${VERSION}-bin.tar.gz

ENV DESTINATION /opt/hbase-${VERSION}

# Folder for data
RUN mkdir -p /data/hbase
RUN mkdir -p /data/zookeeper
RUN mkdir -p /app

WORKDIR /home

ADD data /data/

ADD hbase-site.xml /${DESTINATION}/conf/hbase-site.xml
ADD hbase-server.sh /${DESTINATION}/conf/hbase-server.sh

ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
ENV PATH $PATH:/${DESTINATION}/bin

# REST API
EXPOSE 8080
# Thrift API
EXPOSE 9090

# Zookeeper port
EXPOSE 2181

# Master port
EXPOSE 16000
# Master info port
EXPOSE 16010

# Regionserver port
EXPOSE 16020
# Regionserver info port
EXPOSE 16030

WORKDIR ${DESTINATION}
ENTRYPOINT ["sh","conf/hbase-server.sh"]

#CMD ["spark-submit", "/app/contagem_palavras.py"]