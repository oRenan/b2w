import time
from flask import Flask
from flask import jsonify
from pfiles.Actions import Actions

app = Flask(__name__)

@app.route("/")
def hello():
    return "Contagem de Palavras - API REST"

@app.route("/getText/<value>", methods=['GET'])
def getText(value):
    start_time = time.time()
    a = Actions().getText(value)

    jsonConsoles = {
        "palavra": value,
        "qtd":  int(a[b'valores:qtd']) if len(a) > 0 else 0,
        "tempo": time.time() - start_time
    }
    return jsonify(jsonConsoles)

if __name__ == '__main__':
    app.run(debug=False,host='0.0.0.0')

