  ##Solução Proposta
Para facilitar a consulta do usuário com o menor processamento possível e com 
maior velocidade, foi escolhido um processamento batch antes do consumo do dado.
Com o cálculo já efetuado previamente, a consulta se torna simples de ser feita via API.
Também tive a preocupação de escolher ferramentas escaláveis, segue desenho abaixo:

![alt text](./ARQUITETURA.png)

#####Spark
O processamento batch foi feito através do Spark. O Spark possui uma solução de 
processamento distribuído, o que torna simple caso ocorra um aumento na quantidade 
de arquivos a serem processados.

#####HBase
O HBase é um banco de dados NoSQL do tipo chave valor que, assim comoo  Spark também possui a vantagem de ser distribuído.
Como a consulta final é feita por uma palavra inteira, e não parte dela, 
as palavras são armazenadas como chave no HBase, isso torna as consultas muito rapidas.

#####Python Flask
O FLask é uma API muito simples e eficiente para páginas Web. Com ela eu consigo muito rapidamente fazer a criação da API,além disso o Python possui outras bibliotecas simples para consumo do HBase.

## Docker componse
#####Para iniciar os jobs basta executar o comando abaixo na pasta raiz do projeto.
    docker-compose up
    
O comando acima faz a criação de três docker containers:
1. **Spark**. Responsável por efetuar o processamento batch dos arquivos fazendo a 
contagem de palavras.
2. **HBase**. Banco de dados NOSQL do tipo chave valor. O mesmo irá fazer 
consultas das palavras de forma rápdia.
3. **API REST**. Endpoint responsável em fazer a interface entre o usuário e
 os dados armazenados no HBASE.  

####Build e Deploy
Nesse caso eu proponho soluções DevOPS simples utilizando o Jenkins.
O Jenkins pode simplesmente consumir o projeto do Git como também fazer a execução dos scripts em spark.

#####Arquivos Importante
1. Na pasta script existe o script **contagem_palavras.py**. O mesmo faz o processamento dos arquivos e armazena no HBASE logo em seguida.
2. O arquivo **app.py** e a pasta pfiles são a API REST.